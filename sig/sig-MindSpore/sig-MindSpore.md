
# Application to create a new SIG
English | [简体中文](./sig-template_cn.md)


Note: The Charter of this SIG follows the convention described in the openEuler charter [README](/en/governance/README.md), and follows [SIG-governance](/en/technical-committee/governance/SIG-governance.md).

## SIG Mission and Scope

Describe the Mission and objectives of the new SIG, including but not limited to:

- Why create a new SIG in openEuler
  - In order for MindSpore to run stably on openEuler in the future, by establishing the MindSpore project SIG, the open source software that MindSpore relies on is maintained on src-openEuler. 
- The scope of the SIG
  - Expand the AI training framework ecology of the openEuler community
  - Build the open-source software that MindSpore relies on to maintain the ability in openEuler
  - Provide AI training framework software open source platform 
- Which SIGs in openEuler to coorperate with
  - None.


### Deliverables

What and in what form the SIG is responsible for delivering

- Source and tar

### Repositories and description managed by this SIG

- [okhttp](https://gitee.com/src-openeuler/okhttp)

- [sentencepiece](https://gitee.com/src-openeuler/sentencepiece)


### Cross-domain and external-oriented processes

Cross-domain and externally-oriented processes and actions defined and implemented by this SIG:

- Non-Internal Process Checklist
  - Q&A by issues.
  - Contributors can pull-request their efforts, SIG will decide wether to pull the updates.
- The organization guidance plan for the entire openEulerSIG owned by this SIG, etc.
  - None.

